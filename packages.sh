#!/bin/bash

add-apt-repository -y ppa:hertg/egpu-switcher

apt update
apt install -y i3 rofi acpi curl gawk fish powerline feh compton unclutter numlockx blueman arandr autorandr egpu-switcher fonts-font-awesome xbacklight steam-installer apt-transport-https htop grub-customizer virtualbox xcb-proto mpd lxappearance arc-theme numix-icon-theme-circle gnome-flashback
