#!/bin/python3

import os
import re
import json
import subprocess
from subprocess import Popen

DEVICES = {}
DEVICES_ID = list(map(lambda s: re.split('\t', s.decode().strip()),
	Popen("pactl list sources short", shell=True, stdout=subprocess.PIPE).stdout.readlines()
	)
)

DEVICES_NAME = list(map(lambda s: s.decode().strip().split('=')[1].strip().strip('"'),
		Popen("pactl list sources | grep -i 'device.description'", shell=True, stdout=subprocess.PIPE).stdout.readlines()
	)
)

for i in range(len(DEVICES_ID)):
	DEVICE_ID_NAME = DEVICES_ID[i][1].split('.')[0]
	DEVICE_TYPE = 'output' if 'output' in DEVICE_ID_NAME else 'input'
	DEVICES[DEVICES_ID[i][0]] = {'id': DEVICES_ID[i][0], 'type': DEVICE_TYPE, 'name': DEVICES_NAME[i]}
	pass

devices_list = list(DEVICES.values())
devices_list.sort(key=lambda dev: dev['type'], reverse=True)
print_device = lambda dev: "{} {} {}".format(dev['id'], "" if dev['type'] == 'output' else "", dev['name'])

ROFI_IN = '\n'.join(list(map(print_device, devices_list)))
ROFI_IN = ROFI_IN[:len(ROFI_IN) - 1]

# Call menu
SEL = Popen(
	"echo '{}' | rofi -dmenu -p 'Select Audio Device' -columns 2 -a 0 -no-custom -font 'icomoon\-feather 10'".format(ROFI_IN),
	shell=True, stdout=subprocess.PIPE
).stdout.readlines()

SEL_ID = list(SEL)[0].decode().split(' ')[0]
SEL_TYPE = DEVICES[SEL_ID]['type']

# Call command
Popen("pacmd-move-all-{} {}".format(SEL_TYPE, SEL_ID), shell=True)
