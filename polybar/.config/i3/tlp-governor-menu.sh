#!/bin/bash

governors="AC\nBAT";
current="$(sudo tlp-stat -s | grep Mode | awk '{print $3}')";
percent="$(acpi | awk '{print $4}' | awk -F "," '{print $1}')";

case $current in
    "battery")  current="BAT";;
    *)          current="AC";;
esac

selected="$(echo -e "$governors" | rofi -lines 2 -dmenu -p "TLP configuration (Current: "$current":"$percent")")"

if [ "$selected" != "" ]; then
    sudo tlp $selected;
fi