#!/bin/bash

DEVICE=$1
pacmd set-default-sink $DEVICE
pactl list sink-inputs short | awk '$1!=$DEVICE{print $1}' | while read line; do pacmd move-sink-input $line $DEVICE; done
