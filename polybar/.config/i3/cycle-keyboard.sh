#!/bin/bash

CURRENT=$(ibus engine)
LAYOUTS=$(ibus read-config | grep -i 'engines-order' | awk 'split($0,a,": "){print a[2]}' | sed -r "s/(\[|\]|'|\ )//g")
IFS=','; LAYOUTSARR=($LAYOUTS); unset IFS;

for i in "${!LAYOUTSARR[@]}"; do
	if [[ "${LAYOUTSARR[$i]}" = "${CURRENT}" ]]; then
		((i = ++i % ${#LAYOUTSARR[@]})) # Increment to get next element and loop if overflow
		ibus engine "${LAYOUTSARR[$i]}"
	fi 
done
