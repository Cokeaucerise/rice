#!/bin/bash

cpuoptions="$(find /etc/cpufreqd.conf.* -maxdepth 0 -type f)"

selected="$(echo -e "$cpuoptions" | rofi -lines 15 -dmenu -p "cpufreq configuration")"

if [ "$selected" != "" ]; then
    sudo ln -sf $selected /etc/cpufreqd.conf
fi

sudo service cpufreqd restart