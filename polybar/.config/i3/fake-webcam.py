#!/usr/bin/env python3
# Python screenshot tool (fullscreen/area selection)
# Tested on:
# Lubuntu 13.04 x86_64
# Gentoo 4.1.7-hardened-r1 x86_64

import sys
import subprocess
import re
from io import StringIO
from Xlib import X, display, Xutil
from subprocess import Popen

# Documentation for python-xlib here:
# http://python-xlib.sourceforge.net/doc/html/index.html

class XSelect:
    def __init__(self, display):
        # X display
        self.d = display

        # Screen
        self.screen = self.d.screen()

        # Draw on the root window (desktop surface)
        self.window = self.screen.root

        # If only I could get this working...
        #cursor = xobject.cursor.Cursor(self.d, Xcursorfont.crosshair)
        #cursor = self.d.create_resource_object('cursor', Xcursorfont.X_cursor)
        cursor = X.NONE

        self.window.grab_pointer(1, X.PointerMotionMask|X.ButtonReleaseMask|X.ButtonPressMask,
                X.GrabModeAsync, X.GrabModeAsync, X.NONE, cursor, X.CurrentTime)

        self.window.grab_keyboard(1, X.GrabModeAsync, X.GrabModeAsync, X.CurrentTime)

        colormap = self.screen.default_colormap
        color = colormap.alloc_color(0, 0, 0)
        # Xor it because we'll draw with X.GXxor function
        xor_color = color.pixel ^ 0xffffff

        self.gc = self.window.create_gc(
            line_width = 1,
            line_style = X.LineSolid,
            fill_style = X.FillOpaqueStippled,
            fill_rule  = X.WindingRule,
            cap_style  = X.CapButt,
            join_style = X.JoinMiter,
            foreground = xor_color,
            background = self.screen.black_pixel,
            function = X.GXxor,
            graphics_exposures = False,
            subwindow_mode = X.IncludeInferiors,
        )

        done    = False
        started = False
        start   = dict(x=0, y=0)
        end     = dict(x=0, y=0)
        last    = None
        drawlimit = 10
        i = 0

        while done == False:
            e = self.d.next_event()

            # Window has been destroyed, quit
            if e.type == X.DestroyNotify:
                sys.exit(0)

            # Mouse button press
            elif e.type == X.ButtonPress:
                # Left mouse button?
                if e.detail == 1:
                    start = dict(x=e.root_x, y=e.root_y)
                    started = True

                # Right mouse button?
                elif e.detail == 3:
                    sys.exit(0)

            # Mouse button release
            elif e.type == X.ButtonRelease:
                end = dict(x=e.root_x, y=e.root_y)
                if last:
                    self.draw_rectangle(start, last)
                done = True
                pass

            # Mouse movement
            elif e.type == X.MotionNotify and started:
                i = i + 1
                if i % drawlimit != 0:
                    continue

                if last:
                    self.draw_rectangle(start, last)
                    last = None

                last = dict(x=e.root_x, y=e.root_y)
                self.draw_rectangle(start, last)
                pass

            # Keyboard key
            elif e.type == X.KeyPress:
                sys.exit(0)

        self.d.flush()

        coords = self.get_coords(start, end)
        if coords['width'] <= 1 or coords['height'] <= 1:
            sys.exit(0)
        else:
            print ('%d %d %d %d %d %d' % (coords['start']['x'], coords['start']['y'], coords['end']['x'], coords['end']['y'], coords['width'], coords['height']))

    def get_coords(self, start, end):
        safe_start = dict(x=0, y=0)
        safe_end   = dict(x=0, y=0)

        if start['x'] > end['x']:
            safe_start['x'] = end['x']
            safe_end['x']   = start['x']
        else:
            safe_start['x'] = start['x']
            safe_end['x']   = end['x']

        if start['y'] > end['y']:
            safe_start['y'] = end['y']
            safe_end['y']   = start['y']
        else:
            safe_start['y'] = start['y']
            safe_end['y']   = end['y']

        return {
            'start': {
                'x': safe_start['x'],
                'y': safe_start['y'],
            },
            'end': {
                'x': safe_end['x'],
                'y': safe_end['y'],
            },
            'width' : safe_end['x'] - safe_start['x'],
            'height': safe_end['y'] - safe_start['y'],
        }

    def draw_rectangle(self, start, end):
        coords = self.get_coords(start, end)
        self.window.rectangle(self.gc,
            coords['start']['x'],
            coords['start']['y'],
            coords['end']['x'] - coords['start']['x'],
            coords['end']['y'] - coords['start']['y']
        )

if __name__ == '__main__':

    import sys

    if len(sys.argv) == 2 and sys.argv[1] == '-S':
        print(Popen("kill \"$(ps -ea | grep -i 'gst-launch' | awk '{print $1}')\"", shell=True, stdout=subprocess.PIPE).stdout.readline().decode())
    elif len(sys.argv) == 2 and sys.argv[1] == '-A':
        XSelect(display.Display())
    else:

        DEVICES = Popen("ls -1 /dev/video*", shell=True, stdout=subprocess.PIPE).stdout.readlines()
        DEVICES = list(map(lambda s: s.decode().strip(), DEVICES))
        ROFI_IN = '\n'.join(DEVICES)

        SEL = Popen(
            "echo '{}' | rofi -dmenu -p 'Select Dummy webcam' -columns 2 -a 0 -no-custom -font 'icomoon\-feather 10'".format(ROFI_IN),
            shell=True, stdout=subprocess.PIPE
        ).stdout.readlines()
        SEL_DEVICE = SEL[0].decode().strip()

        resArea = Popen('python3 %s -A' % sys.argv[0], shell=True, stdout=subprocess.PIPE).stdout.readline().decode().strip('\n')
        coords = resArea.split(' ')
        webcam_stream =  'gst-launch-1.0 ximagesrc startx=%s starty=%s endx=%s endy=%s use-damage=0' % (coords[0], coords[1], coords[2], coords[3])
        webcam_stream += ' ! video/x-raw,framerate=30/1'        # Set framerate
        webcam_stream += ' ! videoscale method=0'               # Set video compression
        webcam_stream += ' ! video/x-raw,width=640,height=480'  # % (coords[4], coords[5])
        webcam_stream += ' ! videoconvert'                      # convert video to next format
        webcam_stream += ' ! video/x-raw,format=YUY2'           # Set video format to webcam compatible
        webcam_stream += ' ! v4l2sink device={}'.format(SEL_DEVICE)       # Output stream to internal video device

        # fake_webcam_stream =  'bash -c "exec -a %s %s"' % ('fake-webcam', webcam_stream)
        
        # Kill existing stream
        result = Popen('python3 %s -S' % sys.argv[0], shell=True, stdout=subprocess.PIPE).stdout.readline().decode().strip('\n')
        #result = Popen("kill \"$(ps -ea | grep -i 'gst-launch' | awk '{print $1}')\"", shell=True, stdout=subprocess.PIPE).stdout.readline()
        # result = Popen("pkill -f fake-webcam", shell=True, stdout=subprocess.PIPE).stdout.readlines()
        print(result)

        # Start new stream with screen snippet
        print(webcam_stream)
        Popen(webcam_stream, shell=True)
