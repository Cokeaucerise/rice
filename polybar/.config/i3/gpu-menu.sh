#!/bin/bash

gpuoptions="$(find /etc/X11/xorg.conf.* -maxdepth 0 -type f)"

currentdriver="$(grep -i driver -i /etc/X11/xorg.conf | awk '{print $2}')"

selected="$(echo -e "$gpuoptions" | rofi -lines 15 -dmenu -p "Xorg GPU configuration (Current: "$currentdriver")")"

if [ "$selected" != "" ]; then
    sudo ln -sf $selected /etc/X11/xorg.conf
fi

if [ "$selected" != "" ]; then
    yes="Yes"
    no="No"
    dmoptions=""$yes"\n"$no""
    selected="$(echo -e "$dmoptions" | rofi -lines 3 -dmenu -p "Restart display-manager?")"
    if [ $selected == $yes ]; then
        sudo service display-manager restart
    fi
fi
