#!/bin/bash

curl -L https://get.oh-my.fish | fish;
omf install bobthefish
cp -rf .config/fish/* ~/.config/fish/
chsh -s /usr/bin/fish
